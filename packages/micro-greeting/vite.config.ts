import react from '@vitejs/plugin-react-swc'
import {defineConfig} from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
    base: 'http://localhost:4273',
    plugins: [react()],
    preview: {
        port: 4273,
    },
    build: {
        rollupOptions: {
            input: 'src/single-spa.tsx',
            output: {
                format: "systemjs",
                entryFileNames: "[name].[format].js"
            },
            preserveEntrySignatures: "strict"
        },
    }
})
