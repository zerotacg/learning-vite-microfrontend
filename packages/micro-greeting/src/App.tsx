import viteLogo from '/vite.svg'
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import {styled} from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import reactLogo from './assets/react.svg'
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

const StyledRoot = styled('div')(({ theme }) => ({
    margin: theme.spacing(2),
}));

const StyledHeader = styled('h1')(({ theme }) => ({
    ...theme.typography.h3,
    marginBottom: theme.spacing(2),
}));
const StyledListItem = styled(ListItem)(({ theme }) => ({
    padding: 0,
    marginBottom: theme.spacing(1),
}));

export function App() {
    const greetings = ['Hello', 'Good morning', 'Good afternoon', 'Good evening'];

    return (
        <Container maxWidth="xl">
            <div>
                <a href="https://reactjs.org" target="_blank">
                    <img src={reactLogo} className="logo react" alt="React logo"/>
                </a>
            </div>
            <Button variant="contained" color="primary">
                Press me primary
            </Button>
            <Button variant="contained" color="secondary">
                Press me secondary
            </Button>
            <StyledRoot>
                <StyledHeader>Greetings</StyledHeader>
                <List>
                    {greetings.map((greeting) => (
                        <StyledListItem key={greeting}>
                            <ListItemText primary={`${greeting}!`} />
                        </StyledListItem>
                    ))}
                </List>
            </StyledRoot>
        </Container>
    );
}

export default App;
