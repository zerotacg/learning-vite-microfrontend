import {BrowserRouter, NavLink} from "react-router-dom";


export function App() {
    return (
        <BrowserRouter basename="/learning-vite-microfrontend/">
            <nav>
                <NavLink to="/">Dasbhoard</NavLink>
                <NavLink to="/greeting">Greetings</NavLink>
            </nav>
        </BrowserRouter>
    );
}

export default App;
