import Paper from "@mui/material/Paper";
import {styled} from "@mui/material/styles";

export const StyledPaper = styled(Paper)(({theme}) => ({
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));
