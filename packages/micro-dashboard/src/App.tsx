import viteLogo from '/vite.svg'
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import {styled} from '@mui/material/styles';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import {Detail} from "./Detail";
import {Overview} from "./Overview";

const StyledRoot = styled('div')(({theme}) => ({
    flexGrow: 1,
    padding: theme.spacing(2),
}));

export function App() {
    return (
        <BrowserRouter basename="/learning-vite-microfrontend/">
            <Container maxWidth="xl">
                <StyledRoot>
                    <div>
                        <a href="https://vitejs.dev" target="_blank">
                            <img src={viteLogo} className="logo" alt="Vite logo"/>
                        </a>
                    </div>
                    <Button variant="contained" color="primary">
                        Press me primary
                    </Button>
                    <Button variant="contained" color="secondary">
                        Press me secondary
                    </Button>
                    <Routes>
                        <Route
                            element={<Overview/>}
                            index
                        />
                        <Route
                            path="/details/:metric"
                            element={<Detail/>}
                        />
                    </Routes>
                </StyledRoot>
            </Container>
        </BrowserRouter>
    );
}

export default App;
