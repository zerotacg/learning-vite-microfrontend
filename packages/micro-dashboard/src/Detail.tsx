import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {useParams} from "react-router-dom";
import {StyledPaper} from "./StyledPaper";

export function Detail() {
    const {metric} = useParams();
    return (
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <Typography variant="h3" component="h1" gutterBottom>
                    Dashboard
                </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
                <StyledPaper>
                    <Typography variant="h4" component="h2" gutterBottom>
                        {metric}
                    </Typography>
                    <Typography variant="h5" component="h2">
                        1000
                    </Typography>
                </StyledPaper>
            </Grid>
        </Grid>
    );
}
