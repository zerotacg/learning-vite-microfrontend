import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {NavLink} from "react-router-dom";
import {StyledPaper} from "./StyledPaper";

export function Overview() {
    return (
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <Typography variant="h3" component="h1" gutterBottom>
                    Dashboard
                </Typography>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
                <NavLink to="/details/Users">
                    <StyledPaper>
                        <Typography variant="h4" component="h2" gutterBottom>
                            Users
                        </Typography>
                        <Typography variant="h5" component="h2">
                            1000
                        </Typography>
                    </StyledPaper>
                </NavLink>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
                <NavLink to="/details/Sales">
                    <StyledPaper>
                        <Typography variant="h4" component="h2" gutterBottom>
                            Sales
                        </Typography>
                        <Typography variant="h5" component="h2">
                            $5000
                        </Typography>
                    </StyledPaper>
                </NavLink>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
                <NavLink to="/details/Orders">
                    <StyledPaper>
                        <Typography variant="h4" component="h2" gutterBottom>
                            Orders
                        </Typography>
                        <Typography variant="h5" component="h2">
                            50
                        </Typography>
                    </StyledPaper>
                </NavLink>
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
                <NavLink to="/details/Products">
                    <StyledPaper>
                        <Typography variant="h4" component="h2" gutterBottom>
                            Products
                        </Typography>
                        <Typography variant="h5" component="h2">
                            500
                        </Typography>
                    </StyledPaper>
                </NavLink>
            </Grid>
        </Grid>
    );
}
